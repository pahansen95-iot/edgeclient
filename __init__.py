#publically available imports
import multiprocessing
import threading
import uuid
import logging as log
import time
from socket import create_connection
from socket import gethostname
import paho.mqtt.client as mqtt
import yaml
import os
#privately available imports
#import alert

_VERSION = "0.2"

"""Publisher publishes to a single topic"""
class Publisher:
    #Constructor
    """the config parameter is a dictionary loaded via PYAML of the edgeclient config"""
    def __init__(self, config):
        #stip the "edgeclient" YAML key if present
        if "edgeclient" in config:
            config = config["edgeclient"]
        #ensure proper value comes through
        assert type(config) is dict,"pubClient was not passed a dictionary for config"
        #ensure we are on correct version
        assert str(config["version"]) == _VERSION,"Incorrect Config File Version; Expected V{}".format(_VERSION)
        #ensure a publish key exists
        assert "publish" in config,"No publish configuration was defined; please check config file or ensure you are passing the correct configuration dictionary"
        #ensure a topic has been defined
        assert "topic" in config["publish"] and str(config["publish"]["topic"]).strip() is not None,"Topic was ommitted or is blank"
        #ensure a QoS has been defined
        assert "qos" in config["publish"] and 0 <= int(config["publish"]["qos"]) <= 2,"QoS was not set correctly or was omitted."
        #ensure queueing has been set
        assert "queue" in config["publish"] and config["publish"]["queue"] is not None, "Message Queueing/Persistance (queue) was not set correctly or was omitted"
        #ensure at least one server has been set
        assert "brokers" in config["publish"] and len(config["publish"]["brokers"]) > 0,"No MQTT Brokers (server) were defined. Please define at least one."

        #load config values
        self.config = config["publish"]
        self.config["qos"] = int(self.config["qos"])
        #check if a clientID was specified, if not then set as hostname
        if "clientID" not in config or str(config["clientID"]).strip() is None:
            self.config["clientID"] = gethostname()

        pipes = multiprocessing.Pipe(duplex=False)
        #public instance variables
        self.quit_pipe = pipes[1] #send a "quit" string over this pipe to gracefully stop the edgeclient
        self.completed_mids = []
        
        #mqtt client object configuration
        self.mqtt_client = mqtt.Client(client_id=self.config["clientID"], clean_session=bool(self.config["queue"]))

        #Callbacks
        self.mqtt_client.on_publish = self._on_publish
        self.mqtt_client.on_disconnect = self._on_disconnect
        self.mqtt_client.on_connect = self._on_connect

        """TODO: Properly set username & Pasword, How to handle secrets?"""
        self.mqtt_client.username_pw_set(username="GPBurdell27", password="1337H4k0r")
        #self.mqtt_client.tls_set()
        
        #private instance variables
        self._quit_listen = pipes[0] #recieving end of the pipe
        self._inform_chans = [] #list of Pipes that will be informed when the edge client quits.
        self._quit = False
        self._thread_group = {}
        self._threads = []
        self._current_server = None #string used for logging purposes
        self._connected = False
        #ToDo Implement Alert (redo alert to be like logging package)
    
        #starts any internal threads
        self._quit_listener()

    #Exported (Public) Functions
    """get_quit_listener returns a unique Pipe (multiprocessing.connection.Connection) which an external process or thread can listen to for a quit statement."""
    def get_quit_listener(self): #returns multiprocessing.Queue
        chans = multiprocessing.Pipe(duplex=False)
        self._inform_chans.append(chans[1])
        return chans[0]

    """Threaded; See connect_blocking"""
    def connect(self): #returns thread object
        thread = threading.Thread(target=self.connect_blocking)
        thread.start()
        self._threads.append(thread)
        return thread

    """connect_blocking connects & maintains the broker connection swapping out to failover brokers if necessary"""
    def connect_blocking(self):
        #add to thread group
        threadUID = "mqttAlive-" + str(uuid.uuid4)
        self._thread_group[threadUID] = True
        #find first available server
        brokerIndex = 0
        brokers = self.config["brokers"]
        
        while True:
            if self._quit:
                self._thread_group[threadUID] = False
                return
            #connect to server
            self.mqtt_client.connect_async(brokers[brokerIndex]["address"],port=int(brokers[brokerIndex]["port"]))
            #start loop
            self.mqtt_client.loop_start()
            
            #wait for quit or connected callback
            datum = time.time()
            while time.time()-datum < 5.0:
                if self._quit:
                    if self._connected:
                        self.mqtt_client.loop_stop()
                        self.mqtt_client.disconnect()
                    else:
                        try:
                            self.mqtt_client.loop_stop()
                        except Exception as e:
                            log.warning("Exception stopping loop: {}".format(str(e)))
                    self._thread_group[threadUID] = False
                    return
                elif self._connected:
                    break
                else:
                    time.sleep(0.1)
            else:
                #update server
                self.mqtt_client.loop_stop()
                brokerIndex += 1
                if brokerIndex == len(brokers):
                    brokerIndex = 0
                continue

            #connection made successfully, wait for disconnect callback
            retryDatum = time.time()
            while True:
                if self._quit:
                    #time to quit
                    if self._connected:
                        self.mqtt_client.loop_stop()
                        self.mqtt_client.disconnect()
                    else:
                        try:
                            self.mqtt_client.loop_stop()
                        except Exception as e:
                            log.warning("Exception stopping loop: {}".format(str(e)))
                    self._thread_group[threadUID] = False
                    return
                elif not self._connected:
                    #disconnected callback has been made
                    self.mqtt_client.loop_stop()
                    #update server
                    brokerIndex += 1
                    if brokerIndex == len(brokers):
                        brokerIndex = 0
                    break
                elif brokerIndex != 0 and time.time()-retryDatum > (5*60):
                    #attempt to reconnect to main server after 5 minutes
                    #disconnect
                    self.mqtt_client.loop_stop()
                    self.mqtt_client.disconnect()
                    #update server
                    brokerIndex = 0
                    break
                else:
                    time.sleep(0.1)

    """Threaded; See connect_blocking"""
    def publish(self, queue):
        thread = threading.Thread(target=self.publish_blocking, args=(queue,))
        thread.start()
        return thread
    
    """publish_blocking publishes messages it recieves on the queue"""
    def publish_blocking(self, queue):
        assert type(queue) is multiprocessing.queues.Queue,"Parameter queue is not of type {}".format(multiprocessing.queues.Queue)

        #add to thread group
        threadUID = str(uuid.uuid4())
        self._thread_group[threadUID] = True
        data = None
        while not self._quit:
            if not queue.empty():
               #get data
                try:
                    data = queue.get_nowait()
                except Exception as e:
                    log.warning("Error Retrieving Data: {}".format(str(e)))
                    continue
                #publish Data
                try:
                    info = self.mqtt_client.publish(self.config["topic"], payload = data, qos=self.config["qos"])
                    #track publish request
                    threading.Thread(target=self._confirm_pub,args=(queue, info.mid, data)).start()
                except ValueError as ve:
                    log.critical("MQTT Misconfiguration: {}".format(str(ve)))
                except Exception as e:
                    log.critical("Error in publishing & data resilliency: {}".format(str(e)))
            else: 
                time.sleep(0.05)
                continue

        self._thread_group[threadUID] = False #Inform that thread has finished

    #Non-Exported (Private) Functions
    """_quit_listener is non blocking. See _quit_listener_blocking"""
    def _quit_listener(self):
        thread = threading.Thread(target=self._quit_listener_blocking)
        thread.start()
        self._threads.append(thread)
        return thread
    
    """_quit_listener_blocking listens for a quit command and then informs all event subscribers when the client object has gracefully quit"""
    def _quit_listener_blocking(self):
        #add to thread group
        threadUID = "quitListener-" + str(uuid.uuid4)
        self._thread_group[threadUID] = True
        val = ""
        while True:
            try:
                val = self._quit_listen.recv()
            except Exception as e:
                log.critical("quitPipe was closed without informing the edgeclient to quit: {}".format(str(e)))
            if "quit" in val: #block till a quit command is given
                self._quit = True
                #wait till all threads have completed
                quit = False
                while not quit:
                    time.sleep(0.01)
                    for thread, alive in self._thread_group.items():
                        if threadUID in thread: #don't consider itself
                            continue
                        elif alive:
                            quit = False
                            break
                        else:
                            quit = True

                for subscriber in self._inform_chans: #inform subscribers that the client is ready for a graceful exit
                    subscriber.send("done")
                
                self._thread_group[threadUID] = False
                return
            else:
                log.warning("Unsupported condition sent through the quitPipe: {}".format(val))

    """_on_publish let's self know a message has been successfully handed off to the broker."""
    def _on_publish(self, client, userdata, mid):
        self.completed_mids.append(mid)

    """_on_connect enables flag that self has connected to the broker"""
    def _on_connect(self, *args):
        self._connected = True

    """_on_disconnect disables flag that self has disconnected from the broker"""
    def _on_disconnect(self, *args):
        self._connected = False

    """_on_confirm_pub tracks a publish request and ensures data is not lost"""
    def _confirm_pub(self, queue, mid, data):
        #add to thread group
        threadUID = str(uuid.uuid4())
        self._thread_group[threadUID] = True

        datum = time.time()
        while time.time()-datum < 10.0:
            if self._quit:
               datum = 0
               continue
            elif mid in self.completed_mids:
                #successfully published message
                self.completed_mids.remove(mid)
                break
            else:
                time.sleep(0.1)
        else: #failed to publish data
            #return data to queue
            try:
                queue.put(data)
            except Exception as e:
                #log data in event error occurs
                log.warning("Could not emplace data back into queue: {}".format(str(e)))
                log.info("Lost Data: {}".format(data))

        self._thread_group[threadUID] = True

"""Subscriber subscribes to a single topic"""
class Subscriber:
    def __init__(self):
        raise Exception("Subscriber Class is not implemented yet")

"""load_config_from_file is a convenience function that returns a dictionary representation of the edgclient YAML config"""
def load_config(filePath):
    assert os.path.isfile(filePath),"The provided filepath is not valid"
    conf = {}
    try:
        with open(filePath) as stream:
            conf = yaml.safe_load(stream)
    except Exception as e:
        log.critical("{} has invalid YAML syntax: {}".format(filePath, str(e)))
        raise e
    #recursive function for finding the edgeclient node expanding any nested dictionaries
    def _find(d):
        for k, v in d.items():
            if "edgeclient" in k:
                return {k:v}
            elif isinstance(v, dict):
                output = _find({k,v})
                if "edgeclient" in output:
                    return output
        #return empty dictionary if nothing found & you cannot expand anymore
        return {}

    return _find(conf)

"""response is a convenience function that takes a server address & returns True if the server is available & False otherwise"""
def response(server): #return bool
    try:
        create_connection(server, timeout=5)
        return True
    except Exception as e:
        if "timed out" in str(e).lower():
            log.debug("Connection to {} timed out.".format(server))
            return False
        if "connection refused" in str(e).lower():
            log.debug("Connection to {} refused.".format(server))
            return False
        else:
            log.debug("edgeclient: Unexpected exception in response(): " + str(e))
            return False

